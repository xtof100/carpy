﻿using Microsoft.AspNetCore.Mvc;

namespace CarpoolManagerMvcWEB.Models
{
    public class UserModel
    {
        public int TblUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string? Salt { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }


        //connection one to many (User --> Address)
        public virtual ICollection<AddressModel>? Addresses { get; set; }


        //connection one to many (User --> Cars)
        public virtual ICollection<CarModel>? Cars { get; set; }

    }
}
