﻿namespace CarpoolManagerMvcWEB.Models
{
    public class PersonsUnderAuthorityModel
    {
        public int TblPersonsUnderAuthorityId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int? TblUserId { get; set; }
    }
}
