﻿using CarpoolManagerMvcWEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CarpoolManagerMvcWEB.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile/Details
        public IActionResult Details()
        {
            int id = HomeController.getLoggedInUserId();

            HttpResponseMessage userResponse = ApiContext.WebClient.GetAsync($"Users/{id}").Result;
            UserModel userModel = userResponse.Content.ReadAsAsync<UserModel>().Result;

            HttpResponseMessage addressResponse = ApiContext.WebClient.GetAsync($"Addresses").Result;
            List<AddressModel> result = addressResponse.Content.ReadAsAsync<List<AddressModel>>().Result;
            List<AddressModel> addressList = result.Where(a => a.TblUserId.Equals(id)).ToList();

            ViewBag.addressList = addressList;
            return View(userModel);
        }

        // GET: Profile/AddAdress
        public IActionResult AddAddress()
        {
            ViewBag.UserId = HomeController.getLoggedInUserId();
            return View();
        }

        // POST: Profile/AddAdress
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddAddress(AddressModel addressModel)
        {
            if (ModelState.IsValid)
            {
                var addressResponse = await ApiContext.WebClient.PostAsJsonAsync<AddressModel>($"Addresses", addressModel);
                return RedirectToAction(nameof(Details));
            }
            ViewBag.UserId = HomeController.getLoggedInUserId();
            return View(addressModel);
        }

        // GET: Profile/EditUserInfo
        public IActionResult EditUserInfo()
        {
            int id = HomeController.getLoggedInUserId();

            HttpResponseMessage userResponse = ApiContext.WebClient.GetAsync($"Users/{id}").Result;
            UserModel userModel = userResponse.Content.ReadAsAsync<UserModel>().Result;

            return View(userModel);
        }

        // POST: Profile/EditUserInfo
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUserInfo(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                int id = HomeController.getLoggedInUserId();
                var response = await ApiContext.WebClient.PutAsJsonAsync<UserModel>($"Users/{id}", userModel);
                return RedirectToAction(nameof(Details));
            }
            return View(userModel);
        }

        // GET: Profile/EditAddress/5
        public IActionResult EditAddress(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Addresses/{id}").Result;
            AddressModel addressModel = response.Content.ReadAsAsync<AddressModel>().Result;

            if (addressModel == null)
            {
                return NotFound();
            }

            return View(addressModel);
        }

        // POST: Profile/EditAddress/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAddress(int id, AddressModel addressModel)
        {
            if (ModelState.IsValid)
            {
                var response = await ApiContext.WebClient.PutAsJsonAsync<AddressModel>($"Addresses/{id}", addressModel);
                return RedirectToAction(nameof(Details));
            }
            return View(addressModel);
        }

        // GET: Profile/DeleteAddress/5
        public IActionResult DeleteAddress(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Addresses/{id}").Result;
            AddressModel addressModel = response.Content.ReadAsAsync<AddressModel>().Result;

            if (addressModel == null)
            {
                return NotFound();
            }

            return View(addressModel);
        }

        // POST: Profile/DeleteAddress/5
        [HttpPost, ActionName("DeleteAddress")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteAddressConfirmed(int id)
        {
            var response = await ApiContext.WebClient.DeleteAsync($"Addresses/{id}");
            return RedirectToAction(nameof(Details));
        }
    }
}
