﻿using CarpoolManagerMvcWEB.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;

namespace CarpoolManagerMvcWEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private static int userId;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginAsync(LoginModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var response = await ApiContext.WebClient.PostAsJsonAsync<LoginModel>($"Login", loginModel);

                if (response.IsSuccessStatusCode)
                {
                    userId = response.Content.ReadAsAsync<int>().Result;

                    string username = loginModel.UserName;
                    string password = loginModel.Password;

                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                    ApiContext.WebClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", svcCredentials);

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError(nameof(loginModel.Password), "Incorrect username or password");
                return View(loginModel);
            }
            return View(loginModel);
        }

        public static int getLoggedInUserId()
        {
            return userId;
        }

        public IActionResult Setup1()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Setup1(UserModel userModel)
        {
            HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Login").Result;
            List<string> userNames = response.Content.ReadAsAsync<List<string>>().Result;
            if (userNames != null && userNames.Count != 0 && userNames.Contains(userModel.UserName))
            {
                ModelState.AddModelError(nameof(userModel.UserName), "Username already exists");
            }
            if (ModelState.IsValid)
            {
                TempData["Setup1"] = JsonConvert.SerializeObject(userModel);
                return RedirectToAction(nameof(Setup2));
            }
            return View(userModel);
        }

        public IActionResult Setup2()
        {
            var user = TempData.Peek("Setup1");
            if (user == null)
            {
                return NotFound();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Setup2(AddressModel addressModel)
        {
            if (ModelState.IsValid)
            {
                var setup1 = TempData["Setup1"];
                if (setup1 == null)
                {
                    return NotFound();
                }
                UserModel? userModel = JsonConvert.DeserializeObject<UserModel>((string)setup1);
                if (userModel == null)
                {
                    return NotFound();
                }
                var userResponse = await ApiContext.WebClient.PostAsJsonAsync<UserModel>($"Users", userModel);

                UserModel? newUSer = await userResponse.Content.ReadFromJsonAsync<UserModel>();
                if (newUSer == null)
                {
                    return RedirectToAction("Home", "Login");
                }

                string username = userModel.UserName;
                string password = userModel.Password;

                string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                ApiContext.WebClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", svcCredentials);

                userId = newUSer.TblUserId;

                addressModel.TblUserId = userId;

                var addressResponse = await ApiContext.WebClient.PostAsJsonAsync<AddressModel>($"Addresses", addressModel);

                return RedirectToAction("Details", "Profile");
            }
            return View(addressModel);
        }

        public ActionResult Index()
        {
            return View();
        }
    } 
}