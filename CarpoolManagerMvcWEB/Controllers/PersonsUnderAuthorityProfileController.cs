﻿using CarpoolManagerMvcWEB.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarpoolManagerMvcWEB.Controllers
{
    public class PersonsUnderAuthorityProfileController : Controller
    {
        //A) Persons under authority Overview
        //----------------------------------------------------------------------------------
        // GET: PersonsUnderAuthorityProfileController
        public ActionResult Index(int userId)
        {
            HttpResponseMessage response = ApiContext.WebClient.GetAsync($"/PersonsUnderAuthority/{userId}").Result;
            List<PersonsUnderAuthorityModel> result = response.Content.ReadAsAsync<List<PersonsUnderAuthorityModel>>().Result;
            TempData["userID"] = userId;

            return View(result);
        }
        //B) USER CRUD actions
            //CREATE---------------------------------------------------------------------------
            // GET: CarProfileController/Create
            [HttpGet]
            public IActionResult Create()
            {
                int userId;

                if (TempData.ContainsKey("userId"))
                {
                    userId = (int)TempData["userId"];
                    ViewBag.UserId = userId;
                }

                return View();
            }

            [HttpPost]
            public async Task<IActionResult> Create(PersonsUnderAuthorityModel personsUnderAuthority)
            {

                if (ModelState.IsValid)
                {
                    var response = await ApiContext.WebClient.PostAsJsonAsync<PersonsUnderAuthorityModel>($"/PersonsUnderAuthority/{personsUnderAuthority.TblUserId}", personsUnderAuthority);
                    return RedirectToAction("Index", "PersonsUnderAuthorityProfile", new { userId = personsUnderAuthority.TblUserId });
                }

                return View();
            }

            //UPDATE / EDIT-----------------------------------------------------------------
            // GET: CarProfileController/Edit/5
            [HttpGet]
            public ActionResult Edit(int id)
            {
                int userId;

                if (TempData.ContainsKey("userId"))
                {
                    userId = (int)TempData["userId"];
                    ViewBag.UserId = userId;
                }

                HttpResponseMessage response = ApiContext.WebClient.GetAsync($"PersonsUnderAuthorities/{id}").Result;
                PersonsUnderAuthorityModel result = response.Content.ReadFromJsonAsync<PersonsUnderAuthorityModel>().Result;

                return View(result);

            }

            // POST: CarProfileController/Edit/5
            [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> Edit(PersonsUnderAuthorityModel personsUnderAuthority)
            {

                if (ModelState.IsValid)
                {

                    var response = await ApiContext.WebClient.PutAsJsonAsync<PersonsUnderAuthorityModel>($"/EditPersonsUnderAuthority", personsUnderAuthority);
                    return RedirectToAction("Index", "PersonsUnderAuthorityProfile", new { userId = personsUnderAuthority.TblUserId });

                }

                return View();
            }

            //DELETE------------------------------------------------------------------------

            [HttpGet]
            public IActionResult Delete(int id)
            {
                HttpResponseMessage response = ApiContext.WebClient.GetAsync($"PersonsUnderAuthorities/{id}").Result;
                PersonsUnderAuthorityModel result = response.Content.ReadFromJsonAsync<PersonsUnderAuthorityModel>().Result;

                return View(result);
            }


            //4>DELETE call to API example
            [HttpPost, ActionName("Delete")]
            public async Task<IActionResult> DeleteComfirmed(int id)
            {
                HttpResponseMessage getResponse = ApiContext.WebClient.GetAsync($"PersonsUnderAuthorities/{id}").Result;
                PersonsUnderAuthorityModel personUnderAuthority = getResponse.Content.ReadFromJsonAsync<PersonsUnderAuthorityModel>().Result;

            if (ModelState.IsValid)
                {
                    var deleteResponse = await ApiContext.WebClient.DeleteAsync($"PersonsUnderAuthorities/{id}");
                    return RedirectToAction("Index", "PersonsUnderAuthorityProfile", new { userId = personUnderAuthority.TblUserId });
                }

                return View();
            }

    }
}
