﻿using CarpoolManagerMvcWEB.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarpoolManagerMvcWEB.Controllers
{
    public class CarProfileController : Controller
    {
        //A) CAR Overview
        //----------------------------------------------------------------------------------
        // GET: CarProfileController
        [HttpGet]
            public ActionResult Index(int userId)
            {
                HttpResponseMessage response = ApiContext.WebClient.GetAsync($"/CarUser/{userId}").Result;
                List<CarModel> result = response.Content.ReadAsAsync<List<CarModel>>().Result;
                TempData["userID"] = userId;
  
                return View(result);
            }

        //B) USER CRUD actions
            //CREATE-------------------------------------------------------------------------
            // GET: CarProfileController/Create
            [HttpGet]
            public IActionResult Create()
            {
                int userId;

                if (TempData.ContainsKey("userId"))
                {
                    userId = (int)TempData["userId"];
                    ViewBag.UserId = userId;
                }
                
                return View();
            }

            [HttpPost]
            public async Task<IActionResult> Create(CarModel car)
            {
            
                if (ModelState.IsValid)
                {
                    var response = await ApiContext.WebClient.PostAsJsonAsync<CarModel>($"/CarUser/{car.TblUserId}",car);
                        return RedirectToAction("Index", "CarProfile", new { userId = car.TblUserId });
                }

            return View();
            }

            //UPDATE / EDIT-----------------------------------------------------------------
            // GET: CarProfileController/Edit/5
            [HttpGet]
            public ActionResult Edit(int id)
            {
                int userId;

                if (TempData.ContainsKey("userId"))
                {
                    userId = (int)TempData["userId"];
                    ViewBag.UserId = userId;
                }

                HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Cars/{id}").Result;
                CarModel result = response.Content.ReadFromJsonAsync<CarModel>().Result;

            return View(result);

            }

            // POST: CarProfileController/Edit/5
            [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> Edit(CarModel car)
            {

                if (ModelState.IsValid)
                {

                    var response = await ApiContext.WebClient.PutAsJsonAsync<CarModel>($"/EditCar", car);
                    return RedirectToAction("Index", "CarProfile", new { userId = car.TblUserId });

                }

                return View();
            }

            //DELETE------------------------------------------------------------------------

            [HttpGet]
            public IActionResult Delete(int id)
            {
                HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Cars/{id}").Result;
                CarModel result = response.Content.ReadFromJsonAsync<CarModel>().Result;

                return View(result);
            }


            //4>DELETE call to API example
            [HttpPost, ActionName("Delete")]
            public async Task<IActionResult> DeleteComfirmed(int id)
            {
                HttpResponseMessage getResponse = ApiContext.WebClient.GetAsync($"Cars/{id}").Result;
                CarModel car = getResponse.Content.ReadFromJsonAsync<CarModel>().Result;

                if (ModelState.IsValid)
                {
                    var deleteResponse = await ApiContext.WebClient.DeleteAsync($"Cars/{id}");
                    return RedirectToAction("Index", "CarProfile", new { userId = car.TblUserId });
                }

                return View();
            }

        //C) DETAILS
        //-------------------------------------------------------------------------------------
        // GET: CarProfileController/Details/5
        public ActionResult Details(int id)
        {
            HttpResponseMessage getResponse = ApiContext.WebClient.GetAsync($"Cars/{id}").Result;
            CarModel car = getResponse.Content.ReadFromJsonAsync<CarModel>().Result;

            return View(car);
        }
    }
}
