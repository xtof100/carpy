﻿using CarpoolManagerMvcWEB.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;

namespace CarpoolManagerMvcWEB.Controllers
{
    public class UserManagmentController : Controller
    {
        //A) Registration
        //----------------------------------------------------------------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Setup1(UserModel userModel)
        {
            HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Login").Result;
            List<string> userNames = response.Content.ReadAsAsync<List<string>>().Result;
            if (userNames != null && userNames.Count != 0 && userNames.Contains(userModel.UserName))
            {
                ModelState.AddModelError("UserName", "UserName already exists");
            }
            if (ModelState.IsValid)
            {
                TempData["Setup1"] = JsonConvert.SerializeObject(userModel);
                return RedirectToAction(nameof(Setup2));
            }
            return View(userModel);
        }

        public IActionResult Setup2()
        {
            var user = TempData.Peek("Setup1");
            if (user == null)
            {
                return NotFound();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Setup2(AddressModel addressModel)
        {
            if (ModelState.IsValid)
            {
                var setup1 = TempData["Setup1"];
                if (setup1 == null)
                {
                    return NotFound();
                }
                UserModel? userModel = JsonConvert.DeserializeObject<UserModel>((string)setup1);
                if (userModel == null)
                {
                    return NotFound();
                }
                var userResponse = await ApiContext.WebClient.PostAsJsonAsync<UserModel>($"Users", userModel);

                UserModel? newUSer = await userResponse.Content.ReadFromJsonAsync<UserModel>();
                if (newUSer == null)
                {
                    return RedirectToAction("Home", "Login");
                }

                string username = newUSer.UserName;
                string password = newUSer.Password;

                string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));

                ApiContext.WebClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", svcCredentials);

                int userId = newUSer.TblUserId;

                addressModel.TblUserId = userId;

                var addressResponse = await ApiContext.WebClient.PostAsJsonAsync<AddressModel>($"Addresses", addressModel);

                return RedirectToAction("Details", "Profile");
            }
            return View(addressModel);
        }

        //B) USER Overview
        //----------------------------------------------------------------------------------
        public IActionResult Index()
        {
            //1>GET call to API example
            HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Users").Result;
            List<UserModel> result = new List<UserModel>();
            if (response.IsSuccessStatusCode)
            {
                result = response.Content.ReadAsAsync<List<UserModel>>().Result;
            }
            return View(result);
        }

        //C) USER CRUD actions
            //CREATE-------------------------------------------------------------------------
            [HttpGet]

            public IActionResult CreateUser()
            {
                return View();
            }

            [HttpPost]

            public async Task<IActionResult> CreateUser(UserModel user)
            {
                if (ModelState.IsValid)
                {
                    var response = await ApiContext.WebClient.PostAsJsonAsync<UserModel>($"Users", user);
                    return RedirectToAction(nameof(Index));
                }

                return View();
            }

            //UPDATE / EDIT-----------------------------------------------------------------
            [HttpGet]
            public IActionResult Edit(int id)
            {
                HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Users/{id}").Result;
                UserModel result = response.Content.ReadFromJsonAsync<UserModel>().Result;

                return View(result);
            }

            
            public async Task<IActionResult> Edit(int id, UserModel user)
            {

                if (ModelState.IsValid)
                {

                    var response = await ApiContext.WebClient.PutAsJsonAsync<UserModel>($"Users/{id}", user);
                    return RedirectToAction(nameof(Index));

                }

                return View();
            }

            //DELETE------------------------------------------------------------------------
            [HttpGet]
            public IActionResult Delete(int id)
            {
                HttpResponseMessage response = ApiContext.WebClient.GetAsync($"Users/{id}").Result;
                UserModel result = response.Content.ReadFromJsonAsync<UserModel>().Result;

                return View(result);
            }


            //4>DELETE call to API example
            [HttpPost, ActionName("Delete")]
            public async Task<IActionResult> DeleteComfirmed(int id)
            {

                if (ModelState.IsValid)
                {
                    var response = await ApiContext.WebClient.DeleteAsync($"Users/{id}");
                    return RedirectToAction(nameof(Index));
                }

                return View();
            }


    //E) DETAILS
    //-------------------------------------------------------------------------------------
            // GET: CarProfileController/Details/5
            public ActionResult CarDetails(int id)
            {
                return RedirectToAction("Index", "CarProfile", new { userId = id });
            }

            // GET: PersonsUnderAuthorityProfileController/Details/5
            public ActionResult PersonsUnderAuthorityDetails(int id)
            {
                return RedirectToAction("Index", "PersonsUnderAuthorityProfile", new { userId = id });
            }


        //F)OTHER
        //------------------------------------------------------------------------------------
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

