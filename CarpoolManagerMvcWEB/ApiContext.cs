﻿using System.Net.Http.Headers;

namespace CarpoolManagerMvcWEB

{
    public class ApiContext
    {
        public static HttpClient WebClient = new HttpClient();

        static ApiContext()
        {
            //Put your API URL here(For it Rightclick on API Project -> properties -> web -> Copy the projectURL)
            WebClient.BaseAddress = new Uri("https://localhost:7227/api/");
            WebClient.DefaultRequestHeaders.Clear();
            WebClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            WebClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
        }
    }
}
