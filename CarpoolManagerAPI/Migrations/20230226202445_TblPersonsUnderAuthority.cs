﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarpoolManagerAPI.Migrations
{
    public partial class TblPersonsUnderAuthority : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Salt",
                table: "TblUsers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateTable(
                name: "TblPersonsUnderAuthority",
                columns: table => new
                {
                    TblPersonsUnderAuthorityId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TblUserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblPersonsUnderAuthority", x => x.TblPersonsUnderAuthorityId);
                    table.ForeignKey(
                        name: "FK_TblPersonsUnderAuthority_TblUsers_TblUserId",
                        column: x => x.TblUserId,
                        principalTable: "TblUsers",
                        principalColumn: "TblUserId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_TblPersonsUnderAuthority_TblUserId",
                table: "TblPersonsUnderAuthority",
                column: "TblUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblPersonsUnderAuthority");

            migrationBuilder.AlterColumn<string>(
                name: "Salt",
                table: "TblUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
