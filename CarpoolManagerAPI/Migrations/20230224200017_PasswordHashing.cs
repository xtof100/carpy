﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarpoolManagerAPI.Migrations
{
    public partial class PasswordHashing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Salt",
                table: "TblUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Salt",
                table: "TblUsers");
        }
    }
}
