﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarpoolManagerAPI.Migrations
{
    /// <inheritdoc />
    public partial class TblAddressFix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblAddresses_TblUsers_TblUserId",
                table: "TblAddresses");

            migrationBuilder.DropIndex(
                name: "IX_TblAddresses_TblUserId",
                table: "TblAddresses");

            migrationBuilder.DropColumn(
                name: "TblUserId",
                table: "TblAddresses");

            migrationBuilder.AddColumn<int>(
                name: "UserTblUserId",
                table: "TblAddresses",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TblAddresses_UserTblUserId",
                table: "TblAddresses",
                column: "UserTblUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TblAddresses_TblUsers_UserTblUserId",
                table: "TblAddresses",
                column: "UserTblUserId",
                principalTable: "TblUsers",
                principalColumn: "TblUserId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TblAddresses_TblUsers_UserTblUserId",
                table: "TblAddresses");

            migrationBuilder.DropIndex(
                name: "IX_TblAddresses_UserTblUserId",
                table: "TblAddresses");

            migrationBuilder.DropColumn(
                name: "UserTblUserId",
                table: "TblAddresses");

            migrationBuilder.AddColumn<int>(
                name: "TblUserId",
                table: "TblAddresses",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TblAddresses_TblUserId",
                table: "TblAddresses",
                column: "TblUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TblAddresses_TblUsers_TblUserId",
                table: "TblAddresses",
                column: "TblUserId",
                principalTable: "TblUsers",
                principalColumn: "TblUserId");
        }
    }
}
