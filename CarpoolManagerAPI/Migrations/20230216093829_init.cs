﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CarpoolManagerAPI.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblDestinationLibrary",
                columns: table => new
                {
                    TblDestinationLibraryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrganizationName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblDestinationLibrary", x => x.TblDestinationLibraryId);
                });

            migrationBuilder.CreateTable(
                name: "TblGroups",
                columns: table => new
                {
                    TblGroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblGroups", x => x.TblGroupId);
                });

            migrationBuilder.CreateTable(
                name: "TblUsers",
                columns: table => new
                {
                    TblUserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblUsers", x => x.TblUserId);
                });

            migrationBuilder.CreateTable(
                name: "TblAddresses",
                columns: table => new
                {
                    TblAddressId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Street = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Number = table.Column<int>(type: "int", nullable: false),
                    UnitNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    City = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PostalCode = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TblUserId = table.Column<int>(type: "int", nullable: true),
                    TblDestinationLibraryId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblAddresses", x => x.TblAddressId);
                    table.ForeignKey(
                        name: "FK_TblAddresses_TblDestinationLibrary_TblDestinationLibraryId",
                        column: x => x.TblDestinationLibraryId,
                        principalTable: "TblDestinationLibrary",
                        principalColumn: "TblDestinationLibraryId");
                    table.ForeignKey(
                        name: "FK_TblAddresses_TblUsers_TblUserId",
                        column: x => x.TblUserId,
                        principalTable: "TblUsers",
                        principalColumn: "TblUserId");
                });

            migrationBuilder.CreateTable(
                name: "TblCars",
                columns: table => new
                {
                    TblCarId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Brand = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LicensePlate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TotalNumberOfSeats = table.Column<int>(type: "int", nullable: false),
                    CompanyCar = table.Column<bool>(type: "bit", nullable: false),
                    TblUserId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblCars", x => x.TblCarId);
                    table.ForeignKey(
                        name: "FK_TblCars_TblUsers_TblUserId",
                        column: x => x.TblUserId,
                        principalTable: "TblUsers",
                        principalColumn: "TblUserId");
                });

            migrationBuilder.CreateTable(
                name: "TblGroupUsers",
                columns: table => new
                {
                    TblGroupUserId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TblGroupId = table.Column<int>(type: "int", nullable: false),
                    TblUserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblGroupUsers", x => x.TblGroupUserId);
                    table.ForeignKey(
                        name: "FK_TblGroupUsers_TblGroups_TblGroupId",
                        column: x => x.TblGroupId,
                        principalTable: "TblGroups",
                        principalColumn: "TblGroupId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TblGroupUsers_TblUsers_TblUserId",
                        column: x => x.TblUserId,
                        principalTable: "TblUsers",
                        principalColumn: "TblUserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TblAddresses_TblDestinationLibraryId",
                table: "TblAddresses",
                column: "TblDestinationLibraryId");

            migrationBuilder.CreateIndex(
                name: "IX_TblAddresses_TblUserId",
                table: "TblAddresses",
                column: "TblUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblCars_TblUserId",
                table: "TblCars",
                column: "TblUserId");

            migrationBuilder.CreateIndex(
                name: "IX_TblGroupUsers_TblGroupId",
                table: "TblGroupUsers",
                column: "TblGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_TblGroupUsers_TblUserId",
                table: "TblGroupUsers",
                column: "TblUserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblAddresses");

            migrationBuilder.DropTable(
                name: "TblCars");

            migrationBuilder.DropTable(
                name: "TblGroupUsers");

            migrationBuilder.DropTable(
                name: "TblDestinationLibrary");

            migrationBuilder.DropTable(
                name: "TblGroups");

            migrationBuilder.DropTable(
                name: "TblUsers");
        }
    }
}
