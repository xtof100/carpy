﻿namespace CarpoolManagerAPI.Models
{
    public class CarModel
    {
        public int TblCarId { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        public string? LicensePlate { get; set; }
        public int TotalNumberOfSeats { get; set; }
        public bool CompanyCar { get; set; }

        public int? TblUserId { get; set; }
    }
}
