﻿using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace CarpoolManagerAPI.Services
{
    public interface IPersonsUnderAuthoritiesService
    {
        public Task<IEnumerable<TblPersonsUnderAuthority>> GetPersonsUnderAuthorityByUser(int userId);
        public Task<ActionResult<TblPersonsUnderAuthority>> PostPersonsUnderAuthorityByUser(PersonsUnderAuthorityModel personsUnderAuthority, int userId);
        public Task<ActionResult<TblPersonsUnderAuthority>> UpdatePersonsUnderAuthority(PersonsUnderAuthorityModel  personsUnderAuthority);
    }
}
