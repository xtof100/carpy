﻿using CarpoolManagerAPI.Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;

namespace CarpoolManagerAPI.Services
{
    public class UserService : IUserService
    {
        public TblUser GetHashedUser(TblUser tblUser)
        {
            byte[] salt = new byte[128 / 8];
            using (var ran = RandomNumberGenerator.Create())
            {
                ran.GetBytes(salt);
            }
            byte[] passwordHash = HashPassword(tblUser.Password, salt);

            tblUser.Password = Convert.ToBase64String(passwordHash);
            tblUser.Salt = Convert.ToBase64String(salt);
            return tblUser;
        }

        private byte[] HashPassword(string password, byte[] salt)
        {
            return KeyDerivation.Pbkdf2(password, salt,
                KeyDerivationPrf.HMACSHA1, 10000, 256 / 8);
        }
    }
}
