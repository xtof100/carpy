﻿using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Models;

namespace CarpoolManagerAPI.Services
{
    public interface ILoginService
    {
        public bool LoginModelValid(LoginModel loginModel);
        public int GetUserId(LoginModel loginModel);
        public IEnumerable<string> GetUserNames();
    }
}
