﻿using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace CarpoolManagerAPI.Services
{
    public class LoginService : ILoginService
    {
        private readonly CarpoolManagerDbContext _context;

        public LoginService(CarpoolManagerDbContext context)
        {
            _context = context;
        }

        public bool LoginModelValid(LoginModel loginModel)
        {
            string username = loginModel.UserName;
            string password = loginModel.Password;

            var tblUser = _context.TblUsers.Where(u => u.UserName.Equals(username)).FirstOrDefault();
            if (tblUser == null)
            {
                return false;
            }
            string? salt = tblUser.Salt;
            if(salt == null)
            {
                return false;
            }

            var hashedPassword = HashPassword(password, Convert.FromBase64String(salt));
            if (hashedPassword.SequenceEqual(Convert.FromBase64String(tblUser.Password)))
            {
                return true;
            }
            return false;
        }

        private byte[] HashPassword(string password, byte[] salt)
        {
            return KeyDerivation.Pbkdf2(password, salt,
                KeyDerivationPrf.HMACSHA1, 10000, 256 / 8);
        }

        public int GetUserId(LoginModel loginModel)
        {
            string username = loginModel.UserName;

            var tblUser = _context.TblUsers.Where(u => u.UserName.Equals(username)).FirstOrDefault();
            if (tblUser == null)
            {
                return 0;
            }
            return tblUser.TblUserId;
        }

        public IEnumerable<string> GetUserNames()
        {
            List<TblUser> users = _context.TblUsers.ToList();
            List<string> userNames = new List<string>();
            foreach (TblUser user in users)
            {
                userNames.Add(user.UserName);
            }
            return userNames;
        }
    }
}
