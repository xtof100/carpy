﻿using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace CarpoolManagerAPI.Services
{
    public interface ICarsService
    {
        public Task<IEnumerable<TblCar>> GetCarsByUser(int userId);
        public Task<ActionResult<TblCar>> PostCarsByUser(CarModel car, int userId);
        public Task<ActionResult<TblCar>> UpdateCar(CarModel car);
    }
}



//1 create Interface
//2 create Service class
//3 adapt controller for injection via constructor
//4 Put service in ioc container -> program.cs