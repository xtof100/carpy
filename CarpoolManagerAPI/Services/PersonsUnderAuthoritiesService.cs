﻿using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarpoolManagerAPI.Services
{
    public class PersonsUnderAuthoritiesService : IPersonsUnderAuthoritiesService
    {
        private readonly CarpoolManagerDbContext _context;


        public PersonsUnderAuthoritiesService(CarpoolManagerDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<TblPersonsUnderAuthority>> GetPersonsUnderAuthorityByUser(int userId)
        {
            return await _context.TblPersonsUnderAuthority.Where(x => x.TblUserId == userId).ToListAsync();
        }

        public async Task<ActionResult<TblPersonsUnderAuthority>> PostPersonsUnderAuthorityByUser(PersonsUnderAuthorityModel personsUnderAuthority, int userId)
        {

            TblPersonsUnderAuthority tblPersonsUnderAuthority = new TblPersonsUnderAuthority();
            tblPersonsUnderAuthority.FirstName = personsUnderAuthority.FirstName;
            tblPersonsUnderAuthority.LastName = personsUnderAuthority.LastName;

            tblPersonsUnderAuthority.User = await _context.TblUsers.FirstOrDefaultAsync(x => x.TblUserId == userId);

            _context.TblPersonsUnderAuthority.Add(tblPersonsUnderAuthority);
            await _context.SaveChangesAsync();

            return tblPersonsUnderAuthority;
        }

        public async Task<ActionResult<TblPersonsUnderAuthority>> UpdatePersonsUnderAuthority(PersonsUnderAuthorityModel personsUnderAuthority)
        {
            //retrieve data
            TblPersonsUnderAuthority tblPersonsUnderAuthority = await _context.TblPersonsUnderAuthority.FirstOrDefaultAsync(x => x.TblPersonsUnderAuthorityId == personsUnderAuthority.TblPersonsUnderAuthorityId);

            //update data
            tblPersonsUnderAuthority.FirstName = personsUnderAuthority.FirstName;
            tblPersonsUnderAuthority.LastName = personsUnderAuthority.LastName;

            //save data
            _context.TblPersonsUnderAuthority.Update(tblPersonsUnderAuthority);
            await _context.SaveChangesAsync();

            return tblPersonsUnderAuthority;
        }
    }
}
