﻿using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarpoolManagerAPI.Services
{
    public class CarsService : ICarsService
    {
        private readonly CarpoolManagerDbContext _context;


        public CarsService(CarpoolManagerDbContext context)
        {
            _context = context;
        }


        public async Task<IEnumerable<TblCar>> GetCarsByUser(int userId)
        {
            return await  _context.TblCars.Where(x => x.TblUserId == userId).ToListAsync();
        }

        public async Task<ActionResult<TblCar>> PostCarsByUser(CarModel car, int userId)
        {
          TblCar tblCar = new TblCar();
            tblCar.TblUserId = userId;
            tblCar.Brand = car.Brand;
            tblCar.Type = car.Type;
            tblCar.LicensePlate = car.LicensePlate;
            tblCar.TotalNumberOfSeats = car.TotalNumberOfSeats;
            tblCar.CompanyCar = car.CompanyCar;

            tblCar.User = await _context.TblUsers.FirstOrDefaultAsync(x => x.TblUserId == userId);

            _context.TblCars.Add(tblCar);
            await _context.SaveChangesAsync();

            return tblCar;
        }

        public async Task<ActionResult<TblCar>> UpdateCar(CarModel car)
        {
            //retrieve data
            TblCar tblCar = await _context.TblCars.FirstOrDefaultAsync(x => x.TblCarId == car.TblCarId); 

            //update data
            tblCar.Brand = car.Brand;
            tblCar.Type = car.Type;
            tblCar.LicensePlate = car.LicensePlate;
            tblCar.TotalNumberOfSeats = car.TotalNumberOfSeats;
            tblCar.CompanyCar = car.CompanyCar;

            //save data
            _context.TblCars.Update(tblCar);
            await _context.SaveChangesAsync();

            return tblCar;
        }
    }
}
