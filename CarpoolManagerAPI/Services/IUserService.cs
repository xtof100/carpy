﻿using CarpoolManagerAPI.Entities;

namespace CarpoolManagerAPI.Services
{
    public interface IUserService
    {
        public TblUser GetHashedUser(TblUser tblUser);
    }
}
