﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Services;
using System.Collections;
using CarpoolManagerAPI.Models;

namespace CarpoolManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly CarpoolManagerDbContext _context;
        private readonly ICarsService _carsService;

        public CarsController(CarpoolManagerDbContext context, ICarsService carsService )
        {
            _context = context;
            _carsService = carsService;
        }

        

        // GET: api/Cars
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TblCar>>> GetTblCars()
        {
            return await _context.TblCars.ToListAsync();
        }

        // GET: api/Cars
        [HttpGet("/CarUser/{userId}")]
        public async Task<IEnumerable<TblCar>> GetCarsByUser(int userId)
        {
            IEnumerable<TblCar> carsList = await _carsService.GetCarsByUser(userId);
           

            return carsList;
        }

        // GET: api/Cars/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TblCar>> GetTblCar(int id)
        {
            var tblCar = await _context.TblCars.FindAsync(id);

            if (tblCar == null)
            {
                return NotFound();
            }

            return tblCar;
        }

        // PUT: api/Cars/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblCar(int id, TblCar tblCar)
        {
            if (id != tblCar.TblCarId)
            {
                return BadRequest();
            }

            _context.Entry(tblCar).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblCarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/Cars/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("/EditCar")]
        public async Task<IActionResult> UpdateCar(CarModel car)
        {
            var carData = await _carsService.UpdateCar(car);
            
            return CreatedAtAction("GetTblCar", new { id = car.TblCarId }, carData);

        }

        // POST: api/Cars
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("/CarUser/{userId}")]
        public async Task<ActionResult<TblCar>> PostTblCar(CarModel car, int userId)
        {
            var carData = await _carsService.PostCarsByUser(car, userId);

            return CreatedAtAction("GetTblCar", new { id = car.TblCarId }, carData);
        }

        // POST: api/Cars
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TblCar>> PostTblCar(TblCar tblCar)
        {
            _context.TblCars.Add(tblCar);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTblCar", new { id = tblCar.TblCarId }, tblCar);
        }

        // DELETE: api/Cars/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblCar(int id)
        {
            var tblCar = await _context.TblCars.FindAsync(id);
            if (tblCar == null)
            {
                return NotFound();
            }

            _context.TblCars.Remove(tblCar);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TblCarExists(int id)
        {
            return _context.TblCars.Any(e => e.TblCarId == id);
        }
    }
}
