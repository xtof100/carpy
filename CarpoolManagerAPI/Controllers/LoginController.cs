﻿using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Services;
using CarpoolManagerAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace CarpoolManagerAPI.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        // POST: api/Login
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> ValidateLogin(LoginModel loginModel)
        {
            if(!_loginService.LoginModelValid(loginModel))
            {
                return BadRequest();
            }
            int userId = _loginService.GetUserId(loginModel);
            return Ok(userId);
        }

        [HttpGet]
        public IEnumerable<string> GetUserNames()
        {
            return _loginService.GetUserNames();
        }
    }
}
