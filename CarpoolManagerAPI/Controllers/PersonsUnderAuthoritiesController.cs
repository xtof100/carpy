﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CarpoolManagerAPI.Entities;
using CarpoolManagerAPI.Services;
using CarpoolManagerAPI.Models;

namespace CarpoolManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsUnderAuthoritiesController : ControllerBase
    {
        private readonly CarpoolManagerDbContext _context;
        private readonly IPersonsUnderAuthoritiesService _personsUnderAuthorityService;


        public PersonsUnderAuthoritiesController(CarpoolManagerDbContext context, IPersonsUnderAuthoritiesService personsUnderAuthorityService)
        {
            _context = context;
            _personsUnderAuthorityService = personsUnderAuthorityService;
        }

        // GET: api/PersonsUnderAuthorities
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TblPersonsUnderAuthority>>> GetTblPersonsUnderAuthority()
        {
            return await _context.TblPersonsUnderAuthority.ToListAsync();
        }

        // GET: api/PersonsUnderAuthorities/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TblPersonsUnderAuthority>> GetTblPersonsUnderAuthority(int id)
        {
            var tblPersonsUnderAuthority = await _context.TblPersonsUnderAuthority.FindAsync(id);

            if (tblPersonsUnderAuthority == null)
            {
                return NotFound();
            }

            return tblPersonsUnderAuthority;
        }

        // GET: api/Cars
        [HttpGet("/PersonsUnderAuthority/{userId}")]
        public async Task<IEnumerable<TblPersonsUnderAuthority>> GetPersonsUnderAuthorityByUser(int userId)
        {
            IEnumerable<TblPersonsUnderAuthority> carsList = await _personsUnderAuthorityService.GetPersonsUnderAuthorityByUser(userId);
            return carsList;
        }

        // PUT: api/Cars/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("/EditPersonsUnderAuthority")]
        public async Task<IActionResult> UpdatePersonsUnderAuthority(PersonsUnderAuthorityModel personsUnderAuthority)
        {
            var data = await _personsUnderAuthorityService.UpdatePersonsUnderAuthority(personsUnderAuthority);

            return CreatedAtAction("GetTblPersonsUnderAuthority", new { id = personsUnderAuthority.TblPersonsUnderAuthorityId }, data);

        }

        // POST: api/Cars
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("/PersonsUnderAuthority/{userId}")]
        public async Task<ActionResult<TblCar>> PostPersonsUnderAuthorityByUser(PersonsUnderAuthorityModel personsUnderAuthority, int userId)
        {
            var data = await _personsUnderAuthorityService.PostPersonsUnderAuthorityByUser(personsUnderAuthority, userId);

            return CreatedAtAction("GetTblPersonsUnderAuthority", new { id = personsUnderAuthority.TblPersonsUnderAuthorityId }, data);
        }

        // PUT: api/PersonsUnderAuthorities/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblPersonsUnderAuthority(int id, TblPersonsUnderAuthority tblPersonsUnderAuthority)
        {
            if (id != tblPersonsUnderAuthority.TblPersonsUnderAuthorityId)
            {
                return BadRequest();
            }

            _context.Entry(tblPersonsUnderAuthority).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblPersonsUnderAuthorityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PersonsUnderAuthorities
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TblPersonsUnderAuthority>> PostTblPersonsUnderAuthority(TblPersonsUnderAuthority tblPersonsUnderAuthority)
        {
            _context.TblPersonsUnderAuthority.Add(tblPersonsUnderAuthority);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTblPersonsUnderAuthority", new { id = tblPersonsUnderAuthority.TblPersonsUnderAuthorityId }, tblPersonsUnderAuthority);
        }

        // DELETE: api/PersonsUnderAuthorities/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTblPersonsUnderAuthority(int id)
        {
            var tblPersonsUnderAuthority = await _context.TblPersonsUnderAuthority.FindAsync(id);
            if (tblPersonsUnderAuthority == null)
            {
                return NotFound();
            }

            _context.TblPersonsUnderAuthority.Remove(tblPersonsUnderAuthority);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TblPersonsUnderAuthorityExists(int id)
        {
            return _context.TblPersonsUnderAuthority.Any(e => e.TblPersonsUnderAuthorityId == id);
        }
    }
}
