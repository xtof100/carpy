﻿namespace CarpoolManagerAPI.Entities
{
    public class TblDestinationLibrary
    {
        public int TblDestinationLibraryId { get; set; } 
        public string OrganizationName { get; set; }
        public string? Email { get; set; }
        public string? Telephone { get; set; }

        //connection one to many (Organization --> Addresses)
        public virtual ICollection<TblAddress>? Adresses { get; set; }

    }
}
