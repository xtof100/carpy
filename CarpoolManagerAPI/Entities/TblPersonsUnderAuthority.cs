﻿namespace CarpoolManagerAPI.Entities
{
    public class TblPersonsUnderAuthority
    {
        public int TblPersonsUnderAuthorityId { get; set; }
        public string FirstName { get; set; }
        public string LastName  { get; set; }

        public int? TblUserId { get; set; }
        public virtual TblUser User { get; set; }
    }
}
