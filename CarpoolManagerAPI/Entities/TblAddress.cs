﻿using Newtonsoft.Json;

namespace CarpoolManagerAPI.Entities
{
    public class TblAddress
    {
        public int TblAddressId { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public string? UnitNumber { get; set; }
        public string City { get; set; }
        public string? State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public int? TblUserId { get; set; }
        public int? TblDestinationLibraryId { get; set; }
        
        [JsonIgnore]
        public virtual TblUser? User { get; set; }

        [JsonIgnore]
        public virtual TblDestinationLibrary? DestinationLibrary { get; set; }
    }
}
