﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace CarpoolManagerAPI.Entities
{
    public class TblUser
    {
        public int TblUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string? Salt { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }


        //connection one to many (User --> Address)
        [JsonIgnore]
        public virtual ICollection<TblAddress>? Addresses { get; set; }


        //connection one to many (User --> Cars)
        [JsonIgnore]
        public virtual ICollection<TblCar>? Cars { get; set; }

        //connection one to many (User --> Persons under authority)
        [JsonIgnore]
        public virtual ICollection<TblPersonsUnderAuthority>? PersonsUnderAuthority { get; set; }
    }
}
