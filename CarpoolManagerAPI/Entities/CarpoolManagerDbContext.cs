﻿
using Microsoft.EntityFrameworkCore;
using CarpoolManagerAPI.Entities;

namespace CarpoolManagerAPI.Entities
{
    public class CarpoolManagerDbContext : DbContext
    {
        public CarpoolManagerDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<TblDestinationLibrary> TblDestinationLibrary { get; set; }
        public DbSet<TblAddress> TblAddresses { get; set; }
        public DbSet<TblUser> TblUsers { get; set; }
        public DbSet<TblCar> TblCars { get; set; }
        public DbSet<TblGroup> TblGroups { get; set; }
        public DbSet<TblGroupUser> TblGroupUsers { get; set; }
        public DbSet<CarpoolManagerAPI.Entities.TblPersonsUnderAuthority> TblPersonsUnderAuthority { get; set; }
    }
}
