﻿using Newtonsoft.Json;

namespace CarpoolManagerAPI.Entities
{
    public class TblGroupUser
    {
        public int TblGroupUserId { get; set; }
        public int TblGroupId { get; set; }
        public int TblUserId { get; set; }
        bool GroupAccepted { get; set; }
        bool UserAccepted { get; set; }
        bool IsAdmin { get; set; }

        [JsonIgnore]
        public virtual TblGroup TblGroup { get; set; }

        [JsonIgnore]
        public virtual TblUser TblUser { get; set; }
    }
}
