﻿using Newtonsoft.Json;

namespace CarpoolManagerAPI.Entities
{
    public class TblGroup
    {
        public int TblGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public virtual ICollection<TblGroupUser> TblGroupUsers { get; set; }
    }
}
